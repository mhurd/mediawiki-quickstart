#!/bin/bash

set -eu

source "/var/local/installer/php.sh"
source "/var/local/installer/node.sh"
source "/var/local/common/utility.sh"

if [ -z "${GIT_CLONE_BASE_URL:-}" ]; then
  echo "Error: GIT_CLONE_BASE_URL environment variable must be set"
  exit 1
fi

_get_component_type() {
  local component_path="$1"
  dirname "$component_path"
}

_get_component_name() {
  local component_path="$1"
  basename "$component_path"
}

_get_manifest_path() {
  local component_path="$1"
  echo "/var/local/$component_path"
}

_ensure_containers_running() {
  if ! [ "$(docker inspect -f '{{.State.Running}}' mediawiki-mediawiki-1)" = "true" ]; then
    echo "MediaWiki container is not running"
    return 1
  fi
  return 0
}

_is_component_enabled() {
  local component_path="$1"
  local output
  output=$(php /var/local/installer/isComponentEnabled.php --component="$(_get_component_name "$component_path")" --type="$(_get_component_type "$component_path")")
  if [ "$output" == "1" ]; then
    return 0
  else
    return 1
  fi
}

_get_dependencies() {
  local component_path="$1"
  local dependencies_file="$(_get_manifest_path "$component_path")/dependencies.yml"
  if [ -f "$dependencies_file" ]; then
    _yq '.[]' "$(cat "$dependencies_file")"
  else
    echo ""
  fi
}

_get_component_local_settings_path() {
  local component_path="$1"
  echo "$(_get_manifest_path "$component_path")/LocalSettings.php"
}

# Note: caller must declare the following:
#   declare -a INSTALLED_COMPONENTS=()
_install_from_manifest() {
  local component_path="$1"
  local local_settings_path="$(_get_component_local_settings_path "$component_path")"
 
  printf "\n\033[34mInstalling '%s'\033[0m\n" "$(_get_component_name "$component_path")"

  if [ ! -f "$local_settings_path" ]; then
    printf "    \e[31mRequired '%s' does not exist\e[0m\n\
    There is a folder for skins and a folder for extensions\n\
    Ensure your component's manifest folder is in the correct one,\n\
    containing its own 'LocalSettings.php'\n\
\e[31mExiting\e[0m\n" "$local_settings_path"
    exit 1
  fi
 
  if _is_component_enabled "$component_path"; then
    echo "Component '$(_get_component_name "$component_path")' is already installed and active, skipping..."
    return
  fi
 
  local dependencies
  dependencies=$(_get_dependencies "$component_path")
  if [ -n "$dependencies" ]; then
    for dependency in $dependencies; do
      echo "Installing '$(_get_component_name "$component_path")' dependency '$dependency'"
      _install_from_manifest "$dependency"
    done
  fi

  local clone_depth="--depth=${CLONE_DEPTH:-2}"
  if [ "${CLONE_DEPTH:-1}" -eq 0 ]; then
    local clone_depth=""
  fi

  # Generate the repository URL based on component type and name
  local repository
  repository="$GIT_CLONE_BASE_URL/$component_path"

  if ! clone_git_repo "$repository" "$component_path"; then
    echo "Failed to clone repository '$repository'"
    exit 1
  fi
 
  # Include the component's LocalSettings.php in MediaWiki's LocalSettings.php
  echo -e "\n# Local settings for '$(_get_component_name "$component_path")' $(_get_component_type "$component_path")" >> Components.php
  echo "require_once \"$local_settings_path\";" >> Components.php

  INSTALLED_COMPONENTS+=("$component_path")
}

_import_page_dumps_for_installed_components() {
    if [[ ${#INSTALLED_COMPONENTS[@]} -eq 0 ]]; then
        return
    fi
    local folders=()
    for component_path in "${INSTALLED_COMPONENTS[@]}"; do
        local pages_folder="$(_get_manifest_path "$component_path")/pages"
        if [ -d "$pages_folder" ]; then
            folders+=("$pages_folder")
        fi
    done
    if [ ${#folders[@]} -gt 0 ]; then
        bash /import_page_xml.sh "${folders[@]}"
    fi
}

_run_bash_for_installed_components() {
  if [[ ${#INSTALLED_COMPONENTS[@]} -eq 0 ]]; then
    return
  fi
  for component_path in "${INSTALLED_COMPONENTS[@]}"; do
    cd ~
    _run_bash_from_manifest "$component_path" 2>&1 | verboseOrDotPerLine "Running '$component_path/setup.sh' if found"
  done
  cd ~
}

_run_bash_from_manifest() {
  local component_path="$1"
  local setup_script="$(_get_manifest_path "$component_path")/setup.sh"
  echo -e "Looking for '${setup_script}'"
  if [ -f "${setup_script}" ]; then
    echo "Running setup script '${setup_script}'"
    chmod +x "${setup_script}"
    cd ~
    "${setup_script}"
  else
    echo "No '${setup_script}' found, skipping..."
  fi
}

_install_php_and_node_dependencies() {
  if [[ ${#INSTALLED_COMPONENTS[@]} -eq 0 ]]; then
    return
  fi
  if ! install_php_dependencies_for_components; then
    echo "Failed to install php dependencies"
    exit 1
  fi
  if ! install_node_dependencies_for_components "${INSTALLED_COMPONENTS[@]}"; then
    echo "Failed to install node dependencies"
    exit 1
  fi
}

_rebuild_localization_cache() {
  sleep 1
  if [[ ${#INSTALLED_COMPONENTS[@]} -eq 0 ]]; then
    return
  fi
  php maintenance/rebuildLocalisationCache.php --force --threads=$(getconf _NPROCESSORS_ONLN) 2>&1 | verboseOrDotPerLine "Rebuild Mediawiki localization cache"
}

_components_with_compose() {
  for dir in "$@"; do
    if [[ -f "/var/local/$dir/docker-compose.yml" ]]; then
      echo "$dir"
    fi
  done
}

_update_component_docker_compose_includes() {
  local components_file="/var/local/common/docker-compose.components.yml"
  local components
  components=$(_components_with_compose "$@")
  if [[ -z "$components" ]]; then
    return 0
  fi
  local contents
  for component in $components; do
    contents=$(cat "$components_file")
    _yq '.include[0].path = (.include[0].path // []) + ["../'$component'/docker-compose.yml"] | .include[0].path |= unique' "$contents" > "$components_file"
  done
  return 0
}

declare -a INSTALLED_COMPONENTS=()

install() {
  INSTALLED_COMPONENTS=()
  for component_path in "$@"; do
    _install_from_manifest "$component_path"
  done

  # Only proceed with post-install steps if components were actually installed
  if [ ${#INSTALLED_COMPONENTS[@]} -gt 0 ]; then
    _install_php_and_node_dependencies
    _run_bash_for_installed_components
    _rebuild_localization_cache
    _import_page_dumps_for_installed_components
    _update_component_docker_compose_includes "${INSTALLED_COMPONENTS[@]}"
    exit 0
  else
    # Return special exit code to indicate no components needed installation
    exit 2
  fi
}

if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
  install "$@"
fi