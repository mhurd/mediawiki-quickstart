<?php

wfLoadExtension( 'UploadWizard' );
$wgEnableUploads = true;
$wgUseImageMagick = true;
// $wgImageMagickConvertCommand = <path to your convert command>;  # Only needs to be set if different from /usr/bin/convert
