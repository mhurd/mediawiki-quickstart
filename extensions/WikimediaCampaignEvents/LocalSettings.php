<?php

wfLoadExtension( 'WikimediaCampaignEvents' );

$wgWikimediaCampaignEventsFluxxOauthUrl = 'https://wmf.fluxx.io/oauth/token';
$wgWikimediaCampaignEventsFluxxClientID = 'INSERT_CLIENT_ID_HERE';
$wgWikimediaCampaignEventsFluxxClientSecret = 'INSERT_CLIENT_SECRET_HERE';

