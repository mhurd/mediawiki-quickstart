<?php

wfLoadExtension( 'VisualEditor' );
$wgDefaultUserOptions['visualeditor-enable'] = 1;
# Optional: Set it as the default for anonymous users
$wgDefaultUserOptions['visualeditor-editor'] = "visualeditor";
