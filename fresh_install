#!/bin/bash

set -u

source "./config"
source "./common/utility.sh"
source "./docker/docker.sh"
source "./remove"
source "./prepare"
source "./start"
source "./import_page_xml"

fresh_install() {
  verify_compose_version 2 || return 1
  verify_docker_group || return 1
  print_force_mode_notification_if_necessary
  if ! confirm_action "Are you sure you want to do a fresh install"; then
    return
  fi
  remove
  prepare
  start
  import_page_xml "/tmp/page-xml"
}

if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
  start_time=$(date +%s)
  fresh_install "$@"
  print_duration_since_start "$start_time" "\n\033[32m⏲\033[0m Total duration: %02d:%02d\n"
fi
