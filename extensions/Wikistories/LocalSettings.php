<?php

wfLoadExtension( 'Wikistories' );

$wgWikistoriesDiscoveryMode = 'beta';
$wgWikistoriesRestDomain = 'wikipedia.org';
$wgUseInstantCommons = true;
