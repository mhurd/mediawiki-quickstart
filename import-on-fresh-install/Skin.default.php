<?php

# Quickstart copies this file into the Mediawiki folder
# it clones. Mediawiki's LocalSettings.php is made to include
# this file. When the default skin is changed via make_skin_default,
# this file is rewritten with the new skin name. Probably do not
# modify this file directly.

$wgDefaultSkin = "vector";