<?php

wfLoadExtension( 'CodeMirror' );
# This configuration enables syntax highlighting by default for all users
$wgDefaultUserOptions['usecodemirror'] = 1;
